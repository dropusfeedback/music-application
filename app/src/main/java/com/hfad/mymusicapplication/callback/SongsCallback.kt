package com.hfad.mymusicapplication.callback

interface SongsCallback {
    fun songsClicked(position:Int)
}