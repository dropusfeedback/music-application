package com.hfad.mymusicapplication.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.IBinder;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hfad.mymusicapplication.R;
import com.hfad.mymusicapplication.activity.MainActivity;
import com.hfad.mymusicapplication.adapter.AllMusicAdapter;
import com.hfad.mymusicapplication.adapter.SongAdapter;
import com.hfad.mymusicapplication.callback.SongsCallback;
import com.hfad.mymusicapplication.model.Song;
import com.hfad.mymusicapplication.services.MusicController;
import com.hfad.mymusicapplication.services.MusicService;
import com.hfad.mymusicapplication.util.Constants;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.util.ArrayList;


public class MusicFragment extends Fragment implements MediaController.MediaPlayerControl {

    //controller
FloatingActionButton fbPause;
    View view;
    private MusicService musicSrv;
    private MusicController controller;
    private boolean musicBound;
    RecyclerView rvAllSongs;
    MediaRecorder recorder;
    //activity and playback pause flags
FloatingActionButton fab;
int flag=2;
    private MediaPlayer player;

    public MusicFragment() {
        // Required empty public constructor
    }
AllMusicAdapter allMusicAdapter;
    IndicatorSeekBar seekBar;
    private ArrayList<Song> songList;
    private ListView songView;
    ImageView imageView2;
    public boolean paused = false, playbackPaused = false;


    // TODO: Rename and change types and number of parameters

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        musicSrv = ((MainActivity) getActivity()).musicSrv;
        controller = ((MainActivity) getActivity()).controller;
        musicBound = ((MainActivity) getActivity()).musicBound;
/*
        ((MainActivity) getActivity()).musicConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                MusicService.MusicBinder binder = (MusicService.MusicBinder) service;
                //get service
                musicSrv = binder.getService();
                //pass list
                musicSrv.setList(Constants.Companion.getSongsList());
                musicBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                musicBound = false;
            }
        };
*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_music, container, false);
        imageView2 = (ImageView) view.findViewById(R.id.imageView2);
        rvAllSongs = (RecyclerView) view.findViewById(R.id.rvAllSongs);
        seekBar = (IndicatorSeekBar) view.findViewById(R.id.indicatorSeekBar);
        fbPause = (FloatingActionButton) view.findViewById(R.id.fbPause);

         recorder = new MediaRecorder();
//        recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        songList = ((MainActivity) getActivity()).songList;
        songList = Constants.Companion.getSongsList();
        musicSrv.setList(songList);
        musicSrv.setSong(Constants.Companion.getClickedPos());
//        Drawable d = new BitmapDrawable(getActivity().getResources(), songList.get(Constants.Companion.getClickedPos()).getImage());
//        imageView2.setImageDrawable(d);

        Glide.with(getActivity()).load(((MainActivity)getActivity()).getBitmap(songList.get(Constants.Companion.getClickedPos()).getAlbumId())).placeholder(R.drawable.ic_icons_user_male).centerCrop().diskCacheStrategy(
                DiskCacheStrategy.ALL
        ).thumbnail(0.5f).into(imageView2);

//        Glide.with(getActivity()).load(bmp).into(imageView2);
        musicSrv.playSong();
        if (playbackPaused) {
            setController();
            playbackPaused = false;
        }

        allMusicAdapter=new AllMusicAdapter(songList.subList(Constants.Companion.getClickedPos(),songList.size()-1),(MainActivity)getActivity());
        rvAllSongs.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAllSongs.setHasFixedSize(true);
        rvAllSongs.setAdapter(allMusicAdapter);
        allMusicAdapter.setSongsCallback(new SongsCallback() {
            @Override
            public void songsClicked(int position) {

            }
        });
        musicSrv.setListener(new SongAdapter.SongPickListener() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onSongPicked(int position) {
                Glide.with(getActivity()).load(((MainActivity)getActivity()).getBitmap(songList.get(position).getAlbumId())).placeholder(R.drawable.ic_icons_user_male).centerCrop().diskCacheStrategy(
                        DiskCacheStrategy.ALL
                ).thumbnail(0.5f).into(imageView2);
            }
        });
        seekBar.setMax(musicSrv.getDur());

        fbPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag==1){musicSrv.playSong();
                fbPause.setImageResource(R.drawable.ic_baseline_play_arrow);
                flag=2;}else {
                    musicSrv.pausePlayer();
                    fbPause.setImageResource(R.drawable.ic_baseline_pause);
                    flag=1;
                }
            }
        });
        player=musicSrv.getPlayer();
seekBar.setProgress(getCurrentPosition());
        setController();
        seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
               seekTo(seekBar.getProgress());
            }
        });

//        controller.show();
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            songView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    if (!controller.isShowing()) {
                        controller.show();
                    }
                }
            });
        }*/



        return view;
    }

  /*  @Override
    public void onStart() {
        super.onStart();
        if (((MainActivity) getActivity()).playIntent == null) {
            ((MainActivity) getActivity()).playIntent = new Intent(getActivity(), MusicService.class);
            ((MainActivity) getActivity()).bindService(((MainActivity) getActivity()).playIntent, ((MainActivity) getActivity()).musicConnection, Context.BIND_AUTO_CREATE);
            ((MainActivity) getActivity()).startService(((MainActivity) getActivity()).playIntent);
        }
    }*/

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        if (musicSrv != null && musicBound && musicSrv.isPng())
            return musicSrv.getPosn();
        else return 0;
    }

    @Override
    public int getDuration() {
        if (musicSrv != null && musicBound && musicSrv.isPng())
            return musicSrv.getDur();
        else return 0;
    }

    @Override
    public boolean isPlaying() {
        if (musicSrv != null && musicBound)
            return musicSrv.isPng();
        return false;
    }

    @Override
    public void pause() {
        playbackPaused = true;
        musicSrv.pausePlayer();
    }

    @Override
    public void seekTo(int pos) {
        musicSrv.seek(pos);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void start() {
        if (musicSrv != null) {
            musicSrv.go();
        } else {
            musicSrv = ((MainActivity) getActivity()).musicSrv;
            musicSrv.go();
        }
    }

    //set the controller up
    private void setController() {
        controller = new MusicController(getActivity());
        //set previous and next button listeners
        controller.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNext();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrev();
            }
        });
        //set and show
        controller.setMediaPlayer(this);
        controller.setAnchorView(view.findViewById(R.id.imageView2));
        controller.setEnabled(true);
    }

    private void playNext() {
        musicSrv.playNext();
        if (playbackPaused) {
            setController();
            playbackPaused = false;
        }
        controller.show(0);
    }

    private void playPrev() {
        musicSrv.playPrev();
        if (playbackPaused) {
            setController();
            playbackPaused = false;
        }
        controller.show(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        paused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (paused) {
            setController();
            paused = false;
        }
    }

    @Override
    public void onStop() {
        controller.hide();
        super.onStop();
    }


/*
    public void getSongList() {
        //retrieve song info
        ContentResolver musicResolver = getActivity().getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);

        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int albumArt=musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.DATA);
            //add songs to list
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                String coverPath = musicCursor.getString(4);
                String album=musicCursor.getString(musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ALBUM));
                Long albumId = musicCursor.getLong(musicCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));

                Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
                Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
//                albumcover.setImageDrawable(img);
                Bitmap bitmap=null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), albumArtUri);
                    Drawable img = Drawable.createFromPath(bitmap.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }


                songList.add(new Song(thisId, thisTitle, thisArtist,bitmap,album));
            }
            while (musicCursor.moveToNext());

        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                SongAdapter songAdt = new SongAdapter(MusicFragment.this, songList);
                songView.setAdapter(songAdt);

                songAdt.setListener(new SongAdapter.SongPickListener() {
                    @Override
                    public void onSongPicked(int position) {
                        musicSrv.setSong(position);
                        musicSrv.playSong();
                        if(playbackPaused){
                            setController();
                            playbackPaused=false;
                        }
                        controller.show();
                    }
                });

            }
        });
        ((MainActivity)getActivity()).songList=songList;
    }
*/

    //user song select
    public void songPicked(View view) {
        musicSrv.setSong(Integer.parseInt(view.getTag().toString()));
        musicSrv.playSong();
        if (playbackPaused) {
            setController();
            playbackPaused = false;
        }
        controller.show();
    }


}