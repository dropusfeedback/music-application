package com.hfad.mymusicapplication.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.adapter.AllMusicAdapter
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.util.Constants.Companion.clickedPos
import com.hfad.mymusicapplication.util.Constants.Companion.flag
import com.hfad.mymusicapplication.util.Constants.Companion.songsList
import com.hfad.mymusicapplication.util.Constants.Companion.title


class AllMusicFragment : Fragment() {

    lateinit var rvAllSongs: RecyclerView
    lateinit var allMusicAdapter: AllMusicAdapter
    lateinit var textView:TextView
    lateinit var ivBack:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_all_music, container, false)
        rvAllSongs = view.findViewById(R.id.rvAllSongs)
        textView = view.findViewById(R.id.textView)
        ivBack = view.findViewById(R.id.ivBack)
        if (flag==1){
            songsList=(activity as MainActivity).songList
        }
        textView.text= title
        allMusicAdapter =
            AllMusicAdapter(songsList, (activity as MainActivity))
        rvAllSongs.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = allMusicAdapter
        }

        ivBack.setOnClickListener {
            activity!!.supportFragmentManager.popBackStack()
        }

        allMusicAdapter.setSongsCallback(object : SongsCallback {
            override fun songsClicked(position: Int) {
                clickedPos = position
                (activity as MainActivity).startFragment(MusicFragment())
            }
        })

        return view
    }

    fun onFabClick(view: View?) {
       /* val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            Pair(mCoverView, ViewCompat.getTransitionName(mCoverView)),
            Pair(mTitleView, ViewCompat.getTransitionName(mTitleView)),
            Pair(mTimeView, ViewCompat.getTransitionName(mTimeView)),
            Pair(mDurationView, ViewCompat.getTransitionName(mDurationView)),
            Pair(mProgressView, ViewCompat.getTransitionName(mProgressView)),
            Pair(mFabView, ViewCompat.getTransitionName(mFabView))
        )
        ActivityCompat.startActivity(
            this,
            Intent(this, DetailActivity::class.java),
            options.toBundle()
        )*/
    }

}