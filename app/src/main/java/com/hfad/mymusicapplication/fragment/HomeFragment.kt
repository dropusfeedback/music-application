package com.hfad.mymusicapplication.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.adapter.AlbumAdapter
import com.hfad.mymusicapplication.adapter.AllSongsAdapter
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.model.Song
import com.hfad.mymusicapplication.repository.SongsR
import com.hfad.mymusicapplication.repository.SongsRepo
import com.hfad.mymusicapplication.util.Constants.Companion.flag
import com.hfad.mymusicapplication.util.Constants.Companion.songsList
import com.hfad.mymusicapplication.util.Constants.Companion.title
import layout.ArtistAdapter


class HomeFragment : Fragment() {

    lateinit var rvArtists: RecyclerView
    lateinit var rvAlbums: RecyclerView
    lateinit var rvAllSongs: RecyclerView
    lateinit var songsR: SongsR
    lateinit var allSongsAdapter: AllSongsAdapter
    lateinit var albumAdapter: AlbumAdapter
    lateinit var artistAdapter: ArtistAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        rvArtists = view.findViewById(R.id.rvArtists)
        songsR = SongsRepo((activity as MainActivity).songList)
        rvAlbums = view!!.findViewById(R.id.rvAlbums)
        rvAllSongs = view.findViewById(R.id.rvAllSongs)

        setMusic()


        /*allSongsAdapter = AllSongsAdapter(
            (activity as MainActivity).songList,
            activity as MainActivity
        )*/


        artistAdapter.setSongsCallback(object : SongsCallback {
            override fun songsClicked(position: Int) {
                flag=2
                title=songsR.getSongsByArtist()[position].name
              songsList= songsR.getSongsByArtist()[position].songs as ArrayList<Song>
                (activity as MainActivity).startFragment(AllMusicFragment())
            }
        })

        albumAdapter.setSongsCallback(object : SongsCallback {
            override fun songsClicked(position: Int) {
                flag=3
                title=songsR.getSongsByAlbum()[position].name
                songsList= songsR.getSongsByAlbum()[position].songs as ArrayList<Song>
                (activity as MainActivity).startFragment(AllMusicFragment())
            }
        })



        allSongsAdapter.setSongsCallback(object : SongsCallback {
            override fun songsClicked(position: Int) {
                flag = 1
                title="All Songs"
                (activity as MainActivity).startFragment(AllMusicFragment())
            }
        })

        (activity as MainActivity).setListener(object :MainActivity.SongListener{
            override fun getSongs(list: MutableList<Song>?) {
                songsList= list as ArrayList<Song>
                songsR=SongsRepo(songsList)
               (activity as MainActivity).runOnUiThread {
//                   artistAdapter = ArtistAdapter(songsR.getSongsByArtist())
//                   albumAdapter = AlbumAdapter(songsR.getSongsByAlbum(), activity as MainActivity)
                   allSongsAdapter.notifyDataSetChanged()
                   albumAdapter.notifyDataSetChanged()
                   artistAdapter.notifyDataSetChanged()
               }
            }
        })
        return view
    }

    private fun setMusic() {
        albumAdapter = AlbumAdapter(songsR.getSongsByAlbum(), activity as MainActivity)
        rvAlbums.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = albumAdapter
        }
/*
        if ((activity as MainActivity).songList.size>2){
            allSongsAdapter = AllSongsAdapter(
                (activity as MainActivity).songList.subList(0,3),
                activity as MainActivity
            )
        }else{
*/
        Log.e("songList",(activity as MainActivity).songList.size.toString())
            allSongsAdapter = AllSongsAdapter(
                (activity as MainActivity).songList.take(3),
                activity as MainActivity
            )
//        }
        artistAdapter = ArtistAdapter(songsR.getSongsByArtist())
        rvArtists.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = artistAdapter
        }
        rvAllSongs.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = allSongsAdapter
        }
    }

}