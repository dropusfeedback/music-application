package com.hfad.mymusicapplication.repository

import com.hfad.mymusicapplication.model.AlbumData
import com.hfad.mymusicapplication.model.Artstists
import com.hfad.mymusicapplication.model.Song
import com.hfad.mymusicapplication.model.SongByName
import java.util.ArrayList

class SongsRepo(val songs: ArrayList<Song>):SongsR {
    override fun getSongsByArtist(): List<SongByName> {
        val songList= arrayListOf<SongByName>()
       val artists=getArtist()
           for (i in artists.indices){
               val list = arrayListOf<Song>()
               for (j in songs.indices){
                   if (artists[i].equals(songs[j].artist)){
                       list.add(songs[j])
                   }
               }
               songList.add(SongByName(artists[i],list))
           }


        return songList
    }

    override fun getSongsByAlbum(): List<SongByName> {
        val songList= arrayListOf<SongByName>()
        val artists=songs.map { song->song.album }.distinct()

        for (i in artists.indices){
            val list = arrayListOf<Song>()
            for (j in songs.indices){
                if (artists[i].equals(songs[j].album)){
                    list.add(songs[j])
                }
            }
            songList.add(SongByName(artists[i],list))
        }
        return songList
    }

    override fun getArtist(): List<String> {
        return songs.map { song->song.artist }.distinct()
    }

    override fun getAlbum(): List<String> {
        return songs.map { song->song.album }.distinct()
    }
}