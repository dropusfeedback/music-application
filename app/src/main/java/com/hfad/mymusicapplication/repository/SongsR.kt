package com.hfad.mymusicapplication.repository

import com.hfad.mymusicapplication.model.AlbumData
import com.hfad.mymusicapplication.model.Artstists
import com.hfad.mymusicapplication.model.Song
import com.hfad.mymusicapplication.model.SongByName

interface SongsR {

    fun getSongsByArtist(): List<SongByName>
    fun getSongsByAlbum(): List<SongByName>

    fun getArtist():List<String>
    fun getAlbum(): List<String>
}