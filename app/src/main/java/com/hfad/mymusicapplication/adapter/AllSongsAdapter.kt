package com.hfad.mymusicapplication.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.model.Song
import com.hfad.mymusicapplication.util.Constants.Companion.clickedPos

class AllSongsAdapter(val songs: List<Song>, val context: MainActivity):RecyclerView.Adapter<AllSongsAdapter.AllSongsViewHolder>() {

    lateinit var callback: SongsCallback
    inner class AllSongsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvArtists=itemView.findViewById<TextView>(R.id.tvArtists)
        val ivAlbum=itemView.findViewById<ImageView>(R.id.ivAlbum)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllSongsViewHolder {
        return AllSongsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.album_row,
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: AllSongsViewHolder, position: Int) {
      if (position<songs.size-1){  holder.tvArtists.text=songs[position].title
        /*var d: Drawable? = BitmapDrawable(context.resources, songs[position].image)
            holder.ivAlbum.background=d*/
          //        Drawable d = new BitmapDrawable(getActivity().getResources(), songList.get(Constants.Companion.getClickedPos()).getImage());
//        imageView2.setImageDrawable(d);

          Glide.with(context).load(context.getBitmap(songs[position].albumId)) .placeholder(R.drawable.ic_icons_user_male).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).thumbnail(0.5f).into(holder.ivAlbum)
      }else{
          holder.tvArtists.text="All Songs"
          holder.ivAlbum.setBackgroundResource(R.drawable.ic_icon_music)
      }
        holder.itemView.setOnClickListener {
            if (position==2){
                callback.songsClicked(position)
            }
        }

    }
fun setSongsCallback(callback: SongsCallback){
    this.callback=callback
}
    override fun getItemCount(): Int {
        return songs.size
    }
}