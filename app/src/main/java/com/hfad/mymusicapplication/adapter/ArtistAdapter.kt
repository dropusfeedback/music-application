package layout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.model.SongByName

class ArtistAdapter(val list:List<SongByName>):RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>() {

    lateinit var callback: SongsCallback
    val colors = intArrayOf(R.color.blue_color,R.color.green_color,R.color.red_color,R.color.orande_color,R.color.light_blue_color)

    inner class ArtistViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
val tvArtists=itemView.findViewById<TextView>(R.id.tvArtists)
        val llartists=itemView.findViewById<LinearLayout>(R.id.llartists)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistViewHolder {
return ArtistViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.artists_row,parent,false))
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
holder.tvArtists.text=list[position].name
       if (position>0){ val value:Int=list.size%position
       if (value<colors.size){ holder.llartists.setBackgroundResource(colors[value])}
       }
        holder.itemView.setOnClickListener {
            callback.songsClicked(position)
        }

    }

    override fun getItemCount(): Int {
return list.size
    }

    fun setSongsCallback(callback: SongsCallback){
        this.callback=callback
    }

}