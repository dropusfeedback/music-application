package com.hfad.mymusicapplication.adapter

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.model.Song
import java.io.File


class AllMusicAdapter(val songs: List<Song>, val context: MainActivity):RecyclerView.Adapter<AllMusicAdapter.AllMusicHolder>() {
    lateinit var callback: SongsCallback

    inner class AllMusicHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        //get title and artist views
        var songView = itemView.findViewById<View>(R.id.song_title) as TextView
        var artistView = itemView.findViewById<View>(R.id.song_artist) as TextView
        var image = itemView.findViewById<View>(R.id.image) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllMusicHolder {
        return AllMusicHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.all_music_row,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AllMusicHolder, position: Int) {
        holder.songView.text=songs[position].title
        /*var d: Drawable? = BitmapDrawable(context.resources, songs[position].image)
        holder.image.background=d*/

        Glide.with(context).load(context.getBitmap(songs[position].albumId)).placeholder(R.drawable.ic_icons_user_male).centerCrop().diskCacheStrategy(
            DiskCacheStrategy.ALL
        ).thumbnail(0.5f).into(holder.image)
holder.artistView.text=songs[position].artist

        holder.itemView.setOnClickListener {
            callback.songsClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return songs.size
    }

    fun setSongsCallback(callback: SongsCallback){
        this.callback=callback
    }

}