package com.hfad.mymusicapplication.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.callback.SongsCallback
import com.hfad.mymusicapplication.model.Artstists
import com.hfad.mymusicapplication.model.Song
import com.hfad.mymusicapplication.model.SongByName
import java.util.ArrayList

class AlbumAdapter(val artists: List<SongByName>, val context: MainActivity):RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {
    lateinit var callback: SongsCallback

    inner class AlbumViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvArtists=itemView.findViewById<TextView>(R.id.tvArtists)
        val ivAlbum=itemView.findViewById<ImageView>(R.id.ivAlbum)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        return AlbumViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.album_row,
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.tvArtists.text=artists[position].name
        if (artists[position].songs.size>0)
        {/*var d: Drawable? = BitmapDrawable(context.resources, artists[position].songs[0].image)
        holder.ivAlbum.background=d*/

            Glide.with(context).load(context.getBitmap(artists[position].songs[0].albumId)).placeholder(R.drawable.ic_icons_user_male).centerCrop().diskCacheStrategy(
                DiskCacheStrategy.ALL
            ).thumbnail(0.5f).into(holder.ivAlbum)
        }

        holder.itemView.setOnClickListener {
            callback.songsClicked(position)
        }

    }



    override fun getItemCount(): Int {
        return artists.size
    }

    fun setSongsCallback(callback: SongsCallback){
        this.callback=callback
    }

}