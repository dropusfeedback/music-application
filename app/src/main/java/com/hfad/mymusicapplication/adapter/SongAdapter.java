package com.hfad.mymusicapplication.adapter;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hfad.mymusicapplication.fragment.MusicFragment;
import com.hfad.mymusicapplication.R;
import com.hfad.mymusicapplication.model.Song;

import java.util.ArrayList;

public class SongAdapter extends BaseAdapter {
    private ArrayList<Song> songs;
    private LayoutInflater songInf;
    MusicFragment activity;

    public SongAdapter(MusicFragment activity, ArrayList<Song> theSongs){
        songs=theSongs;
        songInf=LayoutInflater.from(activity.getContext());
        this.activity=activity;
    }

    SongPickListener listener;

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LinearLayout songLay = (LinearLayout)songInf.inflate(R.layout.song, viewGroup, false);
        //get title and artist views
        TextView songView = (TextView)songLay.findViewById(R.id.song_title);
        TextView artistView = (TextView)songLay.findViewById(R.id.song_artist);
        ImageView image = (ImageView)songLay.findViewById(R.id.image);

        songLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              listener.onSongPicked(i);
            }
        });
        //get song using position
        Song currSong = songs.get(i);
        //get title and artist strings
        songView.setText(currSong.getTitle());
        artistView.setText(currSong.getArtist());
//        Drawable d = new BitmapDrawable(activity.getResources(), currSong.getImage());
//        image.setBackground(d);
        //set position as tag
        songLay.setTag(i);
        return songLay;
    }

    public void setListener(SongPickListener listener) {
        this.listener = listener;
    }

    public interface SongPickListener{
        void onSongPicked(int position);
    }
}
