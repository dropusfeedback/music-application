package com.hfad.mymusicapplication.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.hfad.mymusicapplication.fragment.HomeFragment;
import com.hfad.mymusicapplication.fragment.MusicFragment;
import com.hfad.mymusicapplication.R;
import com.hfad.mymusicapplication.model.Song;
import com.hfad.mymusicapplication.repository.SongsR;
import com.hfad.mymusicapplication.repository.SongsRepo;
import com.hfad.mymusicapplication.services.MusicController;
import com.hfad.mymusicapplication.services.MusicService;
import com.hfad.mymusicapplication.util.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RequiresApi(api = Build.VERSION_CODES.Q)
public class MainActivity extends AppCompatActivity {

    public MusicService musicSrv;
    public Intent playIntent;
    public MusicController controller;
    public ArrayList<Song> songList = new ArrayList<>();
    public boolean musicBound = false;
    public ServiceConnection musicConnection;
    SongsR songsR;
    MediaMetadataRetriever metaRetriver;
    FragmentTransaction transaction;
    SongListener listener;

    public static String[] projection = new String[]{MediaStore.Audio.AudioColumns.TITLE,
            MediaStore.Audio.AudioColumns._ID, // 0
            MediaStore.Audio.AudioColumns.TRACK, // 1
            MediaStore.Audio.AudioColumns.YEAR, // 2
            MediaStore.Audio.AudioColumns.DURATION, // 3
            MediaStore.Audio.AudioColumns.DATA, // 4
            MediaStore.Audio.AudioColumns.ALBUM, // 4
            MediaStore.Audio.AudioColumns.ALBUM_ID, // 5
            MediaStore.Audio.AudioColumns.ARTIST_ID, // 6
            MediaStore.Audio.AudioColumns.ARTIST/*, MediaStore.Audio.Albums.ALBUM_ART*/};
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        musicConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                MusicService.MusicBinder binder = (MusicService.MusicBinder) service;
                //get service
                musicSrv = binder.getService();
                //pass list
                Constants.Companion.setSongsList(songList);
                musicSrv.setList(Constants.Companion.getSongsList());
                musicBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                musicBound = false;
            }
        };
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        new Thread() {
            @Override
            public void run() {
                super.run();
                progressBar.setVisibility(View.VISIBLE);
                getSongList();

            }
        }.start();

    }

    public Bitmap getBitmap(Long albumId) {
        Bitmap bitmap = null;
        try {
            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), albumArtUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void startFragment(Fragment musicFragment) {
        transaction = getSupportFragmentManager().beginTransaction();
        int count = getSupportFragmentManager().getBackStackEntryCount();
        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

        transaction.addToBackStack(null);
        if (count == 0) {
            transaction.add(R.id.container, musicFragment).commit();
        } else {
            transaction.replace(R.id.container, musicFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count < 2) {
            super.onBackPressed();
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void setListener(SongListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForegroundService(playIntent);
//            }else {
            startService(playIntent);
//            }
        }
    }

    @Override
    public void onDestroy() {
        stopService(playIntent);
        musicSrv = null;
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_shuffle:
                //shuffle
                musicSrv.playSong();
                controller.show();
                musicSrv.setShuffle();
                break;
            case R.id.action_end:
                stopService(playIntent);
                musicSrv = null;
                System.exit(0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getSongList() {
        //retrieve song info
        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);

        if (musicCursor != null && musicCursor.moveToFirst()) {
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int albumArt = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.DATA);
            int duration = musicCursor.getColumnIndex
                    (MediaStore.Audio.AudioColumns.DURATION);
//            int albumIds = musicCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            //add songs to list
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                String coverPath = musicCursor.getString(albumArt);
                String durations = musicCursor.getString(duration);
                String album = musicCursor.getString(musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ALBUM));
                Long albumId = musicCursor.getLong(musicCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID));
//                String albumData = musicCursor.getString(albumIds);
                Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
                Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
               /* metaRetriver = new MediaMetadataRetriever();
                metaRetriver.setDataSource(albumArtUri.getPath());
                byte[] art;
                art = metaRetriver.getEmbeddedPicture();
                Bitmap songImage = BitmapFactory.decodeByteArray(art, 0, art.length);*/
//                albumcover.setImageDrawable(img);
                Bitmap bitmap = null;
               /* try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), albumArtUri);
                    Drawable img = Drawable.createFromPath(bitmap.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/


                songList.add(new Song(thisId, thisTitle, thisArtist, coverPath, album, albumId));
                if (listener != null) {
                    listener.getSongs(songList);
                }
            }
            while (musicCursor.moveToNext());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    songsR = new SongsRepo(songList);
                    startFragment(new HomeFragment());
                }
            });
        }
    }

    public interface SongListener {
        void getSongs(List<Song> list);
    }
}