package com.hfad.mymusicapplication.model

import android.graphics.Bitmap

data class AlbumData(val name:String,val albums:List<String>) {
}
data class Artstists(val name: String,val image:Bitmap,val albumName:String)

data class SongByName(val name:String,val songs:List<Song>)

