package com.hfad.mymusicapplication.model;

import android.graphics.Bitmap;

public class Song {
    private long id;
    private String title;
    private String artist,album;
    private String image;
    Long albumId;
/*
    public Song(long id, String title, String artist) {
        this.id = id;
        this.title = title;
        this.artist = artist;
    }
*/

    public String getImage() {
        return image;
    }

    public Song(long id, String title, String artist, String image, String album,Long albumId) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.image = image;
        this.album = album;
        this.albumId=albumId;
    }

    public Long getAlbumId() {
        return albumId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }
}
