package com.hfad.mymusicapplication.util

import android.app.Application
import android.content.Context

class MusicApplication:Application() {
    companion object{
        lateinit var mContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        mContext= applicationContext
    }
}