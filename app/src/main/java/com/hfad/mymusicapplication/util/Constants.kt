package com.hfad.mymusicapplication.util

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.hfad.mymusicapplication.R
import com.hfad.mymusicapplication.activity.MainActivity
import com.hfad.mymusicapplication.model.Song

class Constants {

    companion object{
        var clickedPos:Int=0
        var flag:Int=0
        var songsList = arrayListOf<Song>()
        var title:String=""
        fun isOreo(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
        }
        const val NAVIGATE_NOWPLAYING = "navigate_nowplaying"

        fun isMarshmallow(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
        }
        fun isLollipop(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        }


        fun isJellyBeanMR2(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
        }

        fun isJellyBeanMR1(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1
        }
        fun getNowPlayingIntent(context: Context?): Intent? {
            val intent = Intent(context, MainActivity::class.java)
            intent.action = NAVIGATE_NOWPLAYING
            return intent
        }

        fun getAlbumArtUri(albumId: Long): Uri? {
            return ContentUris.withAppendedId(
                Uri.parse("content://media/external/audio/albumart"),
                albumId
            )
        }

        fun getBitmap(albumId: Long?, contentResolver: ContentResolver): Bitmap? {
            var bitmap: Bitmap? = null
            try {
                val sArtworkUri = Uri.parse("content://media/external/audio/albumart")
                val albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId!!)
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, albumArtUri)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return bitmap
        }

        fun makeShortTimeString(context: Context, secs: Long): String {
            var secs = secs
            val hours: Long
            val mins: Long
            hours = secs / 3600
            secs %= 3600
            mins = secs / 60
            secs %= 60
            val durationFormat = context.resources.getString(
                if (hours == 0L) R.string.durationformatshort else R.string.durationformatlong
            )
            return String.format(durationFormat, hours, mins, secs)
        }

    }


}